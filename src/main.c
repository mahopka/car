#include "stm32f0xx.h"
#include "adc.h"
#include "tim.h"

extern uint8_t numb_buf;
extern uint16_t DMA_data[256];

float aver_adc1, aver_adc2;

void process_buf(){
	if(numb_buf == 1){
		numb_buf = 0;
		aver_adc1 = 0;
		for(int i = 0; i <= 127; i++){
			aver_adc1 += (float)(DMA_data[i] >> 4) / 128;
		}

		GPIOC->ODR &= ~GPIO_ODR_8;
	}else if(numb_buf == 2){
		numb_buf = 0;
		aver_adc2 = 0;
		for(int i = 128; i < 256; i++){
			aver_adc2 += (float)(DMA_data[i] >> 4) / 128;
		}

		GPIOC->ODR &= ~GPIO_ODR_9;
	}
}


int main(void)
{
  uint32_t i = 0;

  RCC->AHBENR |= RCC_AHBENR_GPIOCEN;
  GPIOC->MODER |= GPIO_MODER_MODER8_0 | GPIO_MODER_MODER9_0;

  adc_init();


  /* Infinite loop */
  while (1)
  {
	 process_buf();
	i++;
  }
}
