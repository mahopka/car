#include "adc.h"
#include <stm32f0xx.h>

uint16_t DMA_data[256];
uint32_t channel_adc = ADC_IN11;

uint8_t numb_buf = 0;

static void adc_init_gpio(uint16_t channels_adc) {
	//adc_in11 PC1
	if (channels_adc & ADC_IN11) {
		RCC->AHBENR |= RCC_AHBENR_GPIOCEN;
		GPIOC->MODER |= GPIO_MODER_MODER1;	//PC1
	}
}

void adc_init(){
	sensor_angle_gpio_init();
	//������������
	//��������� 14 ���
	//
	//������������ �������
	//����������


	//adc_init_gpio(channel_adc);

	RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;

	RCC->CR2 |= RCC_CR2_HSI14ON;
	while(!(RCC->CR2 & RCC_CR2_HSI14RDY));

	ADC1->SMPR |= ADC_SMPR_SMP_2 | ADC_SMPR_SMP_0;
	ADC1->CHSELR = ADC_CHSELR_CHSEL11;	// pc1

	//ADC1->IER |= ADC_IER_EOCIE;
	//NVIC_SetPriority(ADC1_COMP_IRQn, 8);
	//NVIC_EnableIRQ(ADC1_COMP_IRQn);

	ADC1->CFGR1 |= ADC_CFGR1_CONT;

	dma_init();

	ADC1->CR |= ADC_CR_ADEN;
	adc_start();
}

void adc_start(){
	ADC1->CR |= ADC_CR_ADSTART;
}

void ADC1_COMP_IRQHandler(void) {

	ADC1->ISR |= ADC_ISR_EOC;

}

void sensor_angle_gpio_init(){
	RCC->AHBENR |= SENSOR_ANGLE_PORT_RCC;
	SENSOR_ANGLE_PORT->MODER |= 3 << (SENSOR_ANGLE*2);
}

void dma_init(){
	RCC->AHBENR |= RCC_AHBENR_DMA1EN;
	DMA1_Channel1->CNDTR = 256;
	DMA1_Channel1->CPAR = (uint32_t)(&(ADC1->DR));
	DMA1_Channel1->CMAR = (uint32_t)&DMA_data;

	DMA1_Channel1->CCR |= DMA_CCR_PSIZE_0; //16 ���
	DMA1_Channel1->CCR |= DMA_CCR_MSIZE_0;

	DMA1_Channel1->CCR |= DMA_CCR_MINC;
	//DMA1_Channel1->CCR |= DMA_CCR_PINC;

	DMA1_Channel1->CCR |= DMA_CCR_CIRC;

	DMA1_Channel1->CCR |= DMA_CCR_HTIE;
	DMA1_Channel1->CCR |= DMA_CCR_TCIE;

	NVIC_SetPriority(DMA1_Channel1_IRQn, 7);
	NVIC_EnableIRQ(DMA1_Channel1_IRQn);

	ADC1->CFGR1 |= ADC_CFGR1_DMACFG;
	ADC1->CFGR1 |= ADC_CFGR1_DMAEN;

	SYSCFG->CFGR1 |= SYSCFG_CFGR1_ADC_DMA_RMP;

	DMA1_Channel1->CCR |= DMA_CCR_EN;
}

void DMA1_Channel1_IRQHandler(){

	if(DMA1->ISR & DMA_ISR_HTIF1 == DMA_ISR_HTIF1){
		numb_buf = 1;
		DMA1->IFCR |= DMA_IFCR_CHTIF1;

		GPIOC->ODR |=  GPIO_ODR_8;
	}
	if(DMA1->ISR & DMA_ISR_TCIF1 == DMA_ISR_TCIF1){
		numb_buf = 2;
		DMA1->IFCR |= DMA_IFCR_CTCIF1;

		GPIOC->ODR |=  GPIO_ODR_9;
	}

}

