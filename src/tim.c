#include "tim.h"
#include <stm32f0xx.h>


void tim14_init(){
	RCC->APB1ENR |= RCC_APB1ENR_TIM14EN;
	TIM14->ARR |= 8000 - 1;
	TIM14->PSC |= 100 - 1;
	TIM14->DIER |= TIM_DIER_UIE;


	NVIC_SetPriority(TIM14_IRQn, 4);
	NVIC_EnableIRQ(TIM14_IRQn);

	TIM14->CR1 |= TIM_CR1_CEN;
}

void TIM14_IRQHandler(){


	TIM14->SR &= ~(TIM_SR_UIF);
}
