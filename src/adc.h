#ifndef ADC_H_
#define ADC_H_

#include <stm32f0xx.h>
#include "global.h"

void adc_init();
void sensor_angle_gpio_init();
void dma_init();

#define	ADC_IN0		0	//GPIOA
#define	ADC_IN8		2	//GPIOB
#define	ADC_IN11	1	//GPIOC


#endif /* ADC_H_ */
