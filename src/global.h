#ifndef global_h_
#define global_h_
#include <stm32f0xx.h>

#define SENSOR_ANGLE 1 //PA1  ADC_IN1
#define SENSOR_SPEED
#define SENSOR_ANGLE_PORT GPIOA
#define SENSOR_ANGLE_PORT_RCC RCC_AHBENR_GPIOAEN

#endif
